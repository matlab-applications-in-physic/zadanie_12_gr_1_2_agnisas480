% Agnieszka Sasiela
% Written in Matlab2019b 
% with help of Joanna Rocznik
% task 12 

clear                        % first thing to do 
%---------------------------------------------------------------
m=0.01;                      % the mass, kg 
r=0.002 ;                    % radius of the wire, m 
R=0.0508 ;                   % radius of the coil, m 
N=2:1:50;                    % number of coils, from 2 to 50 
shear_m=77.2E9;              % shear modulus/G, Pa  
tau= 2;                      % time constant,s 
beta=1/(2*tau);              % damping coefficient, Hz
g=9.81;                      % alpha= g, m/s^2
omega=50:0.5:400;           % range of angular frequencies 

%-----------------resonance curves-----------------------------

file_ID = fopen('resonance_analysis_results.dat','w'); % creating the new file for writng; writng in the constants used in this task
fprintf(file_ID,' m = %.2fkg  r = %.0dm    R = %.3fm    alpha = g = %.2f m/s^2    tau = %ds    G = %.2d Pa',m,r,R,g,tau,shear_m);
fprintf(file_ID,'\n\n\n');
fclose(file_ID);% closing the file 

for i = 1:size(N,2);% for every number of N 

    k = (shear_m * r^4)/(R^3 * N(i) * 4); % computing k 
    omega_0 = sqrt(k/m); % computing omega_0, which is contingent on k
    
 
    for j = 1:size(omega,2); % When N is constant computing amplitude for different frequencies 
    
        A(j,i) = g/sqrt((omega_0^2 - omega(j)^2)^2 + 4*beta^2*omega(j)^2);
        
    end
    
    % plotting the curves 
    
    subplot(2,1,1);
    f=omega/2/pi;
    plot(f,A);
    grid on;
    grid minor;
    xlabel('f-frequency ,Hz'); 
    xlim([10.5 60]);
    ylabel('Amplitude A,m');

end

%------------------------ resonance amplitude dependence of N  -------------------------------

file_ID = fopen('resonance_analysis_results.dat','a'); % opening the file again to write new data 

for i=1:size(N,2); % computing the maximum amplitude- resonance amplitude for every N 
    
  k = (shear_m * r^4)/(R^3 * N(i) * 4);
  omega_0 = sqrt(k / m);
  Amp(i) = g / sqrt((4 * beta^2 * omega_0^2));
  fprintf(file_ID,'N = %d,   A(N) = %.3f m    f = %.3f Hz  \n',N(i),Amp(i),omega_0); %printing the data to the file
  
end

fclose(file_ID);

% plotting the second graph 

subplot(2,1,2);
plot(N,Amp);
grid on;
grid minor;
title('A(N)');
xlabel('N-number of coils');
ylabel('A-maximum amplitude,m');
xlim([2 50]);
saveas(gcf,'resonance.pdf'); % saving the 2 graphs to PDF file
